import { HttpCode, Injectable } from '@nestjs/common';
import { request } from 'http';
import { find } from 'rxjs';
import { PassThrough } from 'stream';
import { CreateUsersDto } from './dto/create-users.dto';
import { Users } from './interfaces/users.interface';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class UsersService {
  private readonly users: Users[] = [{
    userId: uuidv4(),
    userName: 'john',
    userPassword: 'password',
    userEmail: 'john@mail.com',
  },
  {
    userId: uuidv4(),
    userName: 'maria',
    userEmail: 'maria@mail.com',
    userPassword: 'guess',
  }];

  create(CreateUsersDto: CreateUsersDto) {
    CreateUsersDto.userId = uuidv4();
    this.users.push(CreateUsersDto);
    return this.users;
  }

  findAll(): Users[]{
    return this.users;
  }

  async findOne(useremail: string): Promise<Users | undefined> {
    return this.users.find(user => user.userEmail === useremail);
  }

  update(id: number) {
    return `This action updates a #${id} usersTest`;
  }

  remove(id: number) {
    return `This action removes a #${id} usersTest`;
  }

 
  
}
