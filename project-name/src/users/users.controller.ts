import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe , UsePipes} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUsersDto } from './dto/create-users.dto';
import { Users } from './interfaces/users.interface';
import { ValidationPipe  } from '../validation.pipe';


@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post()
  async create(@Body(new ValidationPipe()) createUsersDto: CreateUsersDto) {
    
    return this.usersService.create(createUsersDto);
  }

  @Get()
  findAll(): Users[]{
    return this.usersService.findAll();
  }

}
