import { IsString, IsInt, IsEmail, IsNotEmpty} from 'class-validator';

export class CreateUsersDto {
  //  @IsInt()
    userId: number;

    @IsString()
    userName: string;

    @IsNotEmpty()
    userPassword: string;

    @IsEmail()
    userEmail: string;
}
