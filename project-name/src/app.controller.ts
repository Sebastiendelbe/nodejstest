
import { Controller, Request, Post, UseGuards, Get , Param, Body, Patch} from '@nestjs/common';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { OpenFoodService } from './openFood/openFood.service';
import { UsersService } from './users/users.service';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';

@Controller()
export class AppController {
  constructor(private authService: AuthService, 
    private openFoodService: OpenFoodService) {}
  

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('product')
  getProfile(@Body('productCode') productCode: string) {
    return  this.openFoodService.findProduct(productCode);
  }
}