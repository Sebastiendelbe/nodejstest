import { Module } from '@nestjs/common';
import { OpenFoodService } from './openFood.service';
import {HttpModule} from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  providers: [OpenFoodService],
  exports: [OpenFoodService],
})
export class OpenFoodModule {}