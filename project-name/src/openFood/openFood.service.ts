import { Injectable } from '@nestjs/common';
import {HttpService, HttpModule} from '@nestjs/axios';
import { map, Observable } from 'rxjs';
import {AxiosResponse } from 'axios'

@Injectable()
export class OpenFoodService {
    constructor(private httpService: HttpService) {}

    findProduct(productCode: string) {
        return this.httpService.get('https://world.openfoodfacts.org/api/v2/product/' + productCode).
        pipe(map(response => response.data));

    }
}