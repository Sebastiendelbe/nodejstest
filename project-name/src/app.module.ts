import { Module } from '@nestjs/common';
import { UsersController } from './users/users.controller';
import { UsersModule } from './users/users.module';
import { UsersService } from './users/users.service';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';
import { OpenFoodModule } from './openFood/openFood.module';

@Module({
  controllers: [AppController],
  imports: [UsersModule, AuthModule, OpenFoodModule],
})
export class AppModule {}
