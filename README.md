# NodeJSTest



## Getting started

This repository is for Innovorder's test.

## How to use it

First, clone the project and run "npm i" in your console.
Wait until installation is finished.
Then you can run the API with 'nest start' command.

You can use 3 paths :
- (POST) http://localhost:3000/users : For creating new user with arguments in the body form (key: userPassword can't be empty, key: userEmail must be an email, key: userName must be a string).

- (POST) http://localhost:3000/auth/login : For Loging with your email and password you just set or take one of the users already created. you need to do it in the body (raw) like : {"username": "john@mail.com", "password": "password"}. A JWT is returning.

- (GET) http://localhost:3000/product : For searching a product in OpenFoodAPI. You need to put your JWT in Authorization section in bearer token. after that, you need to add your poduct code to the body form like : key: productCode and the value for example '04963406').
